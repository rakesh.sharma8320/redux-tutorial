

const inputFields = [
    {
        name: "username",
        label: "Username",
        type: "text"

    },
    {
        name: 'name',
        label: "FullName",
        type: "text"
    },
    {
        name: "website",
        label: "Website",
        type: "text"
    },

    {
        name: "email",
        label: "Email",
        type: "email"
    }
];

export default inputFields;