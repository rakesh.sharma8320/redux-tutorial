import React from 'react';

const User = (props) => {
    // creating array of li of for each user with following properties
    let flag = true;
    const users = props.users.map(user => {
        let color = flag ? "lightgrey" : "white";
        flag = !flag;
        return (
        <li className="list-group-item" key={user.id}
        style = {{
            backgroundColor: color,
            fontFamily: "Yu Gothic"
        }}
        >
            <h5 style = {{textAlign:"left"}}>
                <strong>Username:</strong>  {user.username} <br />
                <strong>Email:</strong>  {user.email} <br />
                <strong>FullName:</strong>  {user.name} <br />
                <strong>Website:</strong>  {user.website} <br />
            </h5>
        </li>);

    });
    
    return (
        <ul className="list-group text-center">
        {/* printing array of li here */}
         { users}
        </ul>
    );
};

export default User;