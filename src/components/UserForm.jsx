import React from 'react';

class UserForm extends React.Component{
    state={
        username:"",
        name:"",
        email:""
    }

    addUser(e){
        e.preventDefault();
        // calling dispatcher to adduser 
        // this function will add user to the state
        // redux will compare the next to prev state 
        // if the new state is different from previous state it will render the container

        // NOTE: MUTATION IS QUITE AN IMPORTANT ASPECT IN REDUX, KEEP IT MIND EVERY TIME UNLESS YOU WILL GET UNDESIRABLE RESULTS
        this.props.addUser(Object.assign({}, this.state, {id: new Date().toTimeString()}))
    }
    onChange(e){
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        })
    }
    render(){
        // console.log(this.props)
        const formItems = this.props.setting.map(item => {
           
            
            return(
                <div key = {item.name} className="form-group">
                    <label htmlFor={item.name}>{item.label}:</label>
                    <input onChange={this.onChange.bind(this)} type={item.type} name= {item.name} id= {item.name} className="form-control" />
                </div>
            )
        })
        console.log(this.state)
        return (
            <form onSubmit={this.addUser.bind(this)}
            style = {{
                backgroundColor:"lightblue"            }}
            className="form-control">
                <h2>Add User</h2>
                
                {formItems}
                <input type="submit" value="add User" className="btn btn-primary" />
            </form>
        );
    }
};

export default UserForm;