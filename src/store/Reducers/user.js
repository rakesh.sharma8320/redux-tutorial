let initialState = {
    fetching: false,
    fetched: false,
    users:[],
    errors: null
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_USER_START":
            return Object.assign({}, state, {fetching: true})
            break;

        case "FETCH_USER_ERROR":
            // return {
            //     ...state,
            //     errors: action.payload,
            //     fetching:false,
            //     fetched: false
            // }
            return Object.assign({}, state, {
                errors: action.payload,
                fetching: false,
                fetched: false 
            })
            break;

        case "FETCH_USER_RECEIVED":
            // let temp = {
            //     ...state,
            //     fetching: false,
            //     fetched: false
            // }
            // temp.users = state.users.concat(...action.payload);
            
            return Object.assign({}, state, {users: state.users.concat(...action.payload)})
            break;

        case "ADD_USER":
        console.log("add User")
            // let obj = {
            //     ...state,
            //     id: new Date().toLocaleTimeString()
            // }
            // obj.users = state.users.concat(action.payload)
            // return obj;
            let obj = Object.assign({}, state ,{
                users: state.users.concat()
            })
            obj.users.unshift(action.payload);
            return obj;
            break;

        default:
            break;
    }
    return state;
}

export default userReducer;