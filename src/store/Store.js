import {applyMiddleware, combineReducers, createStore } from "redux";
// this will log the redux 
import { logger } from "redux-logger";
import userReducer from "./Reducers/user";
import tweetsReducer from "./Reducers/tweets";
import thunk from "redux-thunk";

// applying logger middleware
const middleware  = applyMiddleware(thunk);

// combining user and tweet reducers
const reducers = combineReducers({
    user: userReducer,
    tweets: tweetsReducer
})


// adding reducres initial state and middleware to the store
const store = createStore(reducers, {}, middleware)

export default store;