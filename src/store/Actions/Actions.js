export const fetchUsers = () => (dispatch) =>{
    dispatch({
        type: "FETCH_USER_START",
    })

    fetch('https://jsonplaceholder.typicode.com/users').then(res => res.json()).then(res => {
        dispatch({
            type: "FETCH_USER_RECEIVED",
            payload: res
        })
    }).catch(err => {
        dispatch({
            type: "FETCH_USER_ERROR",
            payload: err
        })
    })
};
export const addUser = (user) => (dispatch) =>{
    dispatch({
        type: "ADD_USER",
        payload: user
    })
};