import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import logo from './logo.svg';
import './App.css';
import store from "./store/Store";
import App from "./App";
import { Provider } from "react-redux";

ReactDOM.render(<Provider store = {store}>
    <App/>
    </Provider>,
document.getElementById('root'));


registerServiceWorker();
