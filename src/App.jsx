import React, { Component } from 'react';

// getting store
import store from "./store/Store";

// connect will add props and dispatcher to the container and will return a HOC
import { connect } from "react-redux";

// getting dispatchers from actions
import {fetchUsers, addUser} from "./store/Actions/Actions";

// dumb components
import Users from "./components/User";
import UserForm from "./components/UserForm";
import inputFields from "./inputFields";


// this component will act as a container
class App extends Component {

  componentWillMount(){
    this.props.fetchUsers();
    console.log("COMPONENTE WILL MOUNT IS CALLED")
    // console.log(this.props)
  }

  render() {
    
    return (
      <div className = "container bg-dark pt-2">
      <h1 style = {{color: "white", textAlign: "center"}}>User Data Application</h1>
      <UserForm addUser = {this.props.addUser} setting = {inputFields }/>
      <hr/>
       
       <hr className = "bg-dark" />
       <hr/>
       <h1 style = {{color:"white", textAlign: "center"}}>User Details</h1>
       <Users users = {this.props.users}/>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    users: store.user.users
  }
}



export default connect(mapStateToProps, {fetchUsers, addUser})(App);
